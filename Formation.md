* Création du projet : ng new NOM_DU_PROJET --strict false --skip-tests true
  1) --strict false : désactive le mode strict
  2) --skip-tests : ne créera pas de fichier .spec.ts (qui servent pour les tests unitaire).
* installation des package utilisé :
  1) ng add @angular/material
  2) ng add @ng-bootstrap/ng-bootstrap
  3) npm install @auth0/angular-jwt
* Démarrer le serveur : ng serve
* Création de components :
  * ng generate component PATH/COMPONENT_NAME
  * ng g c PATH/COMPONENT_NAME
  * ng g c PATH/COMPONENT_NAME --module MODULE_NAME.module
    * --module permet de dire dans quel module on crée le component.
  * Un component sera lié à une balise html custom dans angular (le selector dans le @Component), on pourra donc appeler ou utiliser ce component dans l'html d'autres components.
* Création de service :
  * ng generate services PATH/SERVICE_NAME
  * ng g s PATH/SERVICE_NAME
  * Les services vont surtout nous servir pour communiquer avec nos serveurs.
* Création de module :
  * ng generate module PATH/MODULE_NAME
  * ng g m PATH/MODULE_NAME
  * Les modules servent pour regrouper des components ensemble. Par exemple le MatTableModule comportant le nécéssaire pour les tableau de materal.
* Créer une guard :
  * ng generate guard PATH/GUARD_NAME
  * ng g g PATH/GUARD_NAME
  * Les guards vont nous servir pour sécuriser certains accès à notre application.

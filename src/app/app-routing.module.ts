import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { UserComponent } from './modules/user/pages/user/user.component';
import { UserListComponent } from './modules/user/pages/user-list/user-list.component';
import { DemoComponent } from './components/demo/demo.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', component: DemoComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },

  // Option 1 children 
  // { path: 'person', children: [
  //   { path: '', component: UserListComponent },
  //   { path: 'add', component: UserComponent },
  //   { path: 'edit/:id', component: UserComponent },
  // ] }

  // Option 2 loadChildren (lazy loading)
  { 
    path: 'person', loadChildren: () => import('./modules/user/user.module')
      .then(m => m.UserModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

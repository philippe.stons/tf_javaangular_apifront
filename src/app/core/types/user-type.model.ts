import { Observable } from 'rxjs';
import { User } from './../models/user.model';

export class LoginInputOptions
{
    login: boolean;
    submitCb: (entity: User) => Observable<any>;
}
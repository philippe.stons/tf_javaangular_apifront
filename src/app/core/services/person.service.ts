import { ServerService } from './server.service';
import { Person } from './../models/person.model';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CRUD, CrudConfig } from './crud';

const config: CrudConfig = { path: "person" };

@Injectable({
  providedIn: 'root'
})
export class PersonService extends CRUD<Person> {

  constructor(
    protected server: ServerService
  ) 
  {
    super(server, config);
  }
}

import { Router } from '@angular/router';
import { ServerService } from './server.service';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from '../models/user.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private jwt: JwtHelperService = new JwtHelperService();
  private loggedIn: boolean = false;

  constructor(
    private server: ServerService,
    private router: Router
  ) 
  {
    this.loggedIn = this.checkToken();
  }

  public register(user: User)
  {
    return this.server.post<User>('register', user)
      .pipe(map((user) => {
        return this.saveToken(user);
      }));
  }

  public login(user: User)
  {
    return this.server.post<User>('login', user)
      .pipe(map((user: User) => {
        return this.saveToken(user);
      }))
  }

  private saveToken(user: User)
  {
    if(user.token)
    {
      // !!!! espace derrière Bearer !!!!
      localStorage.setItem('token', user.token.replace('Bearer ', ''));
      localStorage.setItem('user', JSON.stringify(user));
      this.loggedIn = true;
    }
    return this.loggedIn;
  }

  public logout()
  {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    this.loggedIn = false;
    this.router.navigate(['login']);
  }

  public isLoggedIn()
  {
    return this.loggedIn;
  }

  private checkToken()
  {
    const token = localStorage.getItem('token');

    if(!token || this.jwt.isTokenExpired(token))
    {
      this.logout();
      return false;
    }

    return true;
  }
}

import { PersonService } from './../../../../core/services/person.service';
import { Person } from './../../../../core/models/person.model';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  person: Person;
  personForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private personService: PersonService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.personForm = this.fb.group({
      // Déclaration du formControl lastname.
      lastname: [null, 
          [Validators.required, Validators.minLength(3), Validators.maxLength(30)]
        ],
      firstname: [null,
          [Validators.required, Validators.minLength(3), Validators.maxLength(30)]
      ]
    }); 
    // on récupère l'id depuis notre route (dans le router on a déclarer :id qui est récupérer ici)
    const id = parseInt(this.activatedRoute.snapshot.paramMap.get('id'), 10);
    
    this.person = null;
    // on vérifie si l'id est un nombre, si c'est le cas on va l'utiliser pour récupérer la personne concernée
    if(!isNaN(id))
    {
      this._getPerson(id);
    }
  }
  
  ngOnInit(): void {
  }

  /**
   * Nous permet de récupérer une personne
   */
  private _getPerson(id: number)
  {
    this.personService.getById(id).subscribe((p: Person) => 
    {
      this.person = p;
      this.personForm.patchValue(this.person);
    });
  }

  public submit()
  {
    // Si le formulaire est valide
    if(this.personForm.valid)
    {
      // On récupère ses valeurs en les interprétant comme des Person.
      const person = this.personForm.value as Person;

      // Si this.person n'est pas null
      if(this.person)
      {
        // on update car nous sommes en mode édition
        this.personService.update(this.person.id, person)
        .subscribe(() => 
        {
          this.router.navigate(['person'])
        });
      } else 
      {
        // si non nous sommes en mode création.
        this.personService.insert(person)
          .subscribe(() => {
            this.router.navigate(['person'])
          },
          (e) => 
          {
            console.log(e);          
          });
      }
    }
  }
}

import { Router } from '@angular/router';
import { PersonService } from './../../../../core/services/person.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Person } from 'src/app/core/models/person.model';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  // Sera utiliser pour la création du tableau html
  displayColumns: string[] = ['id', 'lastname', 'firstname'];
  // Contiendra les données à afficher dans le datasource.
  dataSource: MatTableDataSource<Person>;
  // la liste de personnes que l'on récupèrera du serveur.
  personList: Person[];

  // Ici les view child vont nous permettre de récupérer des éléments dans le view
  // donc ici le mat-paginator (ligne 44 de la vue).
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private personService: PersonService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.refresh();
  }

  /**
   * Rafraichi les données du dataSource.
   */
  private refresh()
  {
    this.personService.getAll()
      .subscribe((persons) => 
      {
        this.personList = persons;
        this.updateDatasource();
      })
  }

  /**
   * met à jours les données du dataSource.
   */
  private updateDatasource()
  {
    this.dataSource = new MatTableDataSource(this.personList);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  /**
   * Exemple de fonction appelée depuis la vue (ligne 45 de la vue)
   */
  public create()
  {
    this.router.navigate(['person/add']);
  }
}

import { LoginInputOptions } from './../../core/types/user-type.model';
import { User } from './../../core/models/user.model';
import { Router } from '@angular/router';
import { AuthService } from './../../core/services/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  inputOptions: LoginInputOptions = new LoginInputOptions();

  constructor(
    private authService: AuthService,
    private router: Router
  ) {
    this.inputOptions.login = true;
    this.inputOptions.submitCb = (entity: User) => 
      {
        return this.authService.login(entity);
      };
  }

  ngOnInit(): void {
    
  }

  public submit(user: User)
  {
    // this.authService.login(user)
    //   .subscribe((logged) => 
    //   {
    //     console.log(logged ? "logged in!" : "invalid credentials");
    //     if(logged)
    //     {
    //       this.router.navigate(['person']);
    //     }
    //   })
  }
}

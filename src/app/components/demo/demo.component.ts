import { Person } from './../../core/models/person.model';
import { PersonService } from './../../core/services/person.service';
import { environment } from './../../../environments/environment';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit {

  constructor(
    private personService: PersonService
  ) { }

  ngOnInit(): void {
    this.personService.getAll()
      .subscribe((persons: Person[]) => 
      {
        persons.forEach((p) => console.log(p));
      });

    this.personService.getById(1).subscribe((p) => console.log("Get By ID : ", p));

    let person = new Person();
    person.firstname = "Donald";
    person.lastname = "Duck";
    this.personService.insert(person).subscribe((p) => console.log("POST : ", p));

    this.personService.update(2, person).subscribe((p) => console.log("UPDATE : ", p));
    this.personService.delete(2).subscribe((p) => console.log("delete", p));
  }

}

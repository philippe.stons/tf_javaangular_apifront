import { LoginInputOptions } from './../../core/types/user-type.model';
import { AuthService } from './../../core/services/auth.service';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { User } from './../../core/models/user.model';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  @Input() inputOptions: LoginInputOptions;
  //@Output() submitEvent = new EventEmitter<User>();
  userForm: FormGroup;
  hasError: boolean;
  error: string;

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.userForm = this.fb.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
    });
  }

  public submit()
  {
    if(this.userForm.valid)
    {
      const user = this.userForm.value as User;

      this.inputOptions.submitCb(user)
        .subscribe((logged) => 
        {
          if(logged)
          {
            //this.submitEvent.emit(user);
            this.router.navigate(['person']);
          }
        }, (e) => 
        {
          console.error(e);
          this.hasError = true;
          this.error = e.error.message;
        });
      // this.authService.login(user)
      //   .subscribe((logged) => 
      //   {
      //     console.log(logged ? "logged in!" : "invalid credentials");
      //     if(logged)
      //     {
      //       this.router.navigate(['person']);
      //     }
      //   })
      // this.submitEvent.emit(user);
    }
  }
}

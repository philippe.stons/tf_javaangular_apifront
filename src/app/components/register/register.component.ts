import { LoginInputOptions } from './../../core/types/user-type.model';
import { User } from './../../core/models/user.model';
import { Router } from '@angular/router';
import { AuthService } from './../../core/services/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  inputOptions: LoginInputOptions = new LoginInputOptions();

  constructor(
    private authService: AuthService,
    private router: Router
  ) {
    this.inputOptions.login = false;
    this.inputOptions.submitCb = (entity: User) => 
      {
        return this.authService.register(entity);
      };
  }

  ngOnInit(): void {
  }

  public submit(user: User)
  {
    // this.authService.register(user)
    //   .subscribe((logged) => 
    //   {
    //     console.log(logged ? "logged in!" : "invalid credentials");
    //     if(logged)
    //     {
    //       this.router.navigate(['person']);
    //     }
    //   })
  }
}

import { AuthService } from './core/services/auth.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'tfJavaAngularDemoApi';

  constructor(
    private authService: AuthService
  )
  {}

  public isLoggedIn()
  {
    return this.authService.isLoggedIn();
  }

  public logout()
  {
    this.authService.logout();
  }
}
